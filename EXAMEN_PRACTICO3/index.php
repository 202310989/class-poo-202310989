<?php
class colores{
    public $color

    function __construct(){
        echo "tipos de colores";
    }
    function mostrarcolor(){
        $this->color = "morado";
        echo $this->color;
    }
    function __destruct(){
        echo "Es el unico color";
    }
}
class Animal{ 
    public $nombre; 
    private $raza;
    protected $comida;

    function __construct(){
        echo "Datos del Animal";
    } 
    function mostrarnombre(){
        $this->nombre = "Leon";
        echo $this->nombre;
    }

    private function mostrarraza(){
        $this->raza = "carnivoro"; 
        echo $this->raza;
    } 
    protected function mostrarcomida(){
        $this->comida = "carne";
        echo $this->comida;
    }
    function __destruct(){
        echo "fin de los datos del animal";
    }
}
    $obj = new Animal();

    $obj->mostrarnombre();
    $obj->mostrarraza();
    $obj->mostrarcomida();

    $obj1 = new colores();

    $obj1->mostrarcolor();

?>
?>
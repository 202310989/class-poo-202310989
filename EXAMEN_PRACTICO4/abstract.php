<?php

abstract class datos{

    abstract public function ingresarNombre($nombre);
    abstract public function obtenerNombre();
}
class persona extends datos{
    private $mensaje = "me llamo sebastian";
    private $nombre;

    public function mostrar(){
        echo $this->mensaje;
    }
    public function ingresarNombre($nombre){
        $this->nombre = $nombre;
    }
    public function obtenerNombre(){
        echo $this->nombre;
    }
}
$obj = new persona();
$obj->ingresarNombre("sebastian");
$obj->obtenerNombre();



?>
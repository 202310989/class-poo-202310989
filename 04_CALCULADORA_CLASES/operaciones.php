<?php

//clase
class Operaciones{


    //atributo operacion a realizar 
    public $OperacionRealizar = "";

    //metodo suma con parametro a y b 
    public function suma($a,$b){
        return $a+$b;

    }
    public function resta($a,$b){
        return $a-$b;

    }
    public function division($a,$b){
        return $a/$b;

    }
    public function multiplicacion($a,$b){
        return $a*$b;

    }
    //metodo que ejecuta los metodos en base a la propiedad $OperacionRealizar y tiene 2 parametros a y b 
    public function ResultadoOperacion($a,$b){
        
        switch ($this->OperacionRealizar) {
            //funcion para operacion suma 
            case 'suma':
                return $this->suma($a,$b);
                break;
            //funcion para operacion resta     
            case 'resta':
                return $this->resta($a,$b);
                break;
            //funcion para operacion division    
            case 'division':
                return $this->division($a,$b);
                break;
            //funcion para operacion multiplicacion    
            case 'multiplicacion':
                return $this->multiplicacion($a,$b);
                break;
            default:
                return 'Operacion a realizar no definida';
                break;
        }
    }

}






?>
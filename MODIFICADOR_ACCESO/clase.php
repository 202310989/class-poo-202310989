<?php
//CLASE
class Animal{

    //ATRIBUTOS DE LA CLASE 
    public $nombre; 
    private $raza;
    protected $comida;

    //CONSTRUCTOR
    function __construct(){
        echo "Datos del Animal";
    }

    //METODO PUBLIC QUE NOS PERMITE INGRESAR A LA CLASE DESDE CUALQUIER PARTE DE NUESTRO CODIGO 
    function mostrarnombre(){
        $this->nombre = "Leon";
        echo $this->nombre;
    }
    //METODO PRIVATE QUE SOLO NOS PERMITE IGRESAR A LA CLASE POR MEDIO DEL METODO
    private function mostrarraza(){
        $this->raza = "carnivoro";
        echo $this->raza;
    }
    //METODO PROTECTED QUE NOS PERMITE INGRESAR A LA CLASE POR MEDIO DEL ATRIBUTO O METODO 
    protected function mostrarcomida(){
        $this->comida = "carne";
        echo $this->comida;
    }
}
    //INSTANSIACION DE LA CLASE
    $obj = new Animal();

    $obj->mostrarnombre();
    $obj->mostrarraza();
    $obj->mostrarcomida();

?>
?>